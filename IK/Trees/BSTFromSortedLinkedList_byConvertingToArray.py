import math
from typing import List

from IK.Trees.common.BSTFromArray_RecursionApproach import printBST


# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode():
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def createLinkedListFromArray(self, arr):
        head=tail=None
        for i in range(len(arr)):
            if head==None:
                head=ListNode(arr[i])
                tail=head
                continue
            tail.next=ListNode(arr[i])
            tail=tail.next
        return head
            
    def sortedListToBST(self, head: ListNode) -> TreeNode:
        nums=[]
        while(head):
#             nums.append(head.val)
            nums += head.val,
            head=head.next
        print(nums)
        
        def createBST(nums: List[int], start:int, end:int) -> TreeNode:
            if start==end:
                return TreeNode(nums[start])
            if start>end:
                return None
            '''
            if you find mid by following ceil then the tree looks like
              0
             / \
           -3   9
           /   /
         -10  5
            mid=math.ceil((start+end)/2)
            it is balanced as well 
            '''
            '''
            either u can use ceil or you can take exact dividant if you follow (start+end)//2 then the tree looks like
               0
             /  \
           -10   5
              \   \
             -3    9
            which is still balanced.
            '''
            mid=(start+end)//2
            root=TreeNode(nums[mid])
            root.left=createBST(nums,start,mid-1)
            root.right=createBST(nums,mid+1,end)
            return root
        
        root=createBST(nums,0,len(nums)-1)
        return root
    
    
m=Solution()
arr=[-10,-3,0,5,9]
head=m.createLinkedListFromArray(arr)
root=m.sortedListToBST(head) 
printBST(root)     
            
        
        