'''
Created on Jul 10, 2019

@author: bkotamahanti
'''
from IK.Trees.common.BSTFromArray_IterativeApproach import buildBST_Iterative_withNoStack


def preOrderTraversal_Recursion(root):
    if root==None:
        return
    print(root.val)
    preOrderTraversal_Recursion(root.left) 
    preOrderTraversal_Recursion(root.right)


def preOrderTraversal_Iterative(root):
    if root==None:
        return
    s=[]
    curr=root
    while (len(s)!=0 or curr!=None):
        while(curr):
            s.append(curr)
            '''Printing value before navigating to left node. Think that each node as its own sub tree with left and right '''
            print(curr.val)
            curr=curr.left
        ''' the below stmt is equivalent to getting back to state of prev root in preOrderTraversal_Recursion '''
        curr=s.pop() 
        ''' then going to the right child of current root which is equivalent to preOrderTraversal_Recursion(root.right)  '''
        curr=curr.right

def preOrderTraversal_IterativeWithOneWhileLoop(root):
    if root==None:
        return
    s=[]
    curr=root
    while (len(s)!=0 or curr!=None):
        if curr!=None:
            s.append(curr)
            '''Printing value before navigating to left node. Think that each node as its own sub tree with left and right '''
            print(curr.val)
            curr=curr.left
        else:
            ''' the below stmt is equivalent to getting back to state of prev root in preOrderTraversal_Recursion '''
            curr=s.pop() 
            ''' then going to the right child of current root which is equivalent to preOrderTraversal_Recursion(root.right)  '''
            curr=curr.right

# arr=[25,19,23,15,40,39,45]
# root=None
# root=buildBST_Iterative_withNoStack(root,arr)
# preOrderTraversal_Recursion(root)
# print("=========================")
# preOrderTraversal_Iterative(root)
# print("=========================")
# preOrderTraversal_IterativeWithOneWhileLoop(root)

