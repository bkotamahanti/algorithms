'''
Created on Jul 8, 2019

@author: bkotamahanti
'''
# from IK.Trees.common.BSTFromArray_IterativeApproach import buildBST_Iterative_withNoStack
''' Think that each node as its own subtree of left and right '''

from IK.Trees.common.BSTFromArray_IterativeApproach import buildBST_Iterative_withNoStack

def InorderTraversalRecursion(root):
    if not root:
        return
    InorderTraversalRecursion(root.left)
    print(root.val)
    InorderTraversalRecursion(root.right)
    
def InorderTraversal_Iterative_2WhileLoops(root):
    if not root:
        return
    
    curr=root
    s=[]
    while(s or curr):
        while(curr):
            ''' goto the left of every node. Goto left of root, since Inorder is first L, R, Rt you cannot print left.val, you 
            have to go to root.left.left as left node has its own sub tree...and so on. Reach to left most of tree
            the below code  is equivalent to  InorderTraversalRecursion(root.left) '''
            s.append(curr)
            curr=curr.left
        '''printing the root.val of left most leaf node. It is DFT, which is bottom up traversal which means 
        goto the bottom of tree which is leaf and come up to root. '''
        curr=s.pop()
        print(curr.val)
        ''' goto right node of root node '''
        curr=curr.right

def InorderTraversal_Iterative_1WhileLoops(root):
    if not root:
        return
    curr=root
    s=[]
    while(s or curr):
        ''' goto the left of every node. Goto left of root, since Inorder is first L, R, Rt you cannot print left.val, you 
            have to go to root.left.left as left node has its own sub tree...and so on. Reach to left most of tree
            the below code  is equivalent to  InorderTraversalRecursion(root.left) '''
        if curr != None:
            s.append(curr)
            curr=curr.left
        else:    
            '''printing the root.val of left most leaf node. It is DFT, which is bottom up traversal which means 
        goto the bottom of tree which is leaf and come up to root. '''
            curr=s.pop()
            print(curr.val)
            ''' goto right node of root node '''
            curr=curr.right
    
# arr=[25,19,23,15,40,39,45]
# root=None
# root=buildBST_Iterative_withNoStack(root,arr)
# InorderTraversalRecursion(root)
# print("=========================")
# InorderTraversal_Iterative_2WhileLoops(root)
# print("=========================")
# InorderTraversal_Iterative_1WhileLoops(root)


        
    