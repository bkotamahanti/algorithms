'''
Created on Jul 11, 2019

@author: bkotamahanti
'''
'''L Rt R '''

from IK.Trees.common.BSTFromArray_IterativeApproach import buildBST_Iterative_withNoStack

'''Time: O(n) Space: O(logN) but this is stack segment of memory which may lead to stack overflow if the tree is big or skewed'''
def PostorderTraversalRecursion(root):
    if root==None:
        return
    PostorderTraversalRecursion(root.left)
    PostorderTraversalRecursion(root.right)
    print(root.right)

'''Time: O(n) Space: O(logN) but this is Heap segment of memory as we are using built in library list s which uses heap segment internally
which dont lead to stack overflow if the tree is big or skewed since the heap
segment is more compared to stack segment of memory'''
def PostorderTraversal_Iterative(root):
    if root==None:
        return
    curr=root
    s=[]
    while(s or curr):
        if curr!=None:
            s.append(curr)
            curr=curr.left
        else:
            if s[len(s)-1].right == None:
                curr=s.pop()
                print(curr.val)
                while(curr==s[len(s)-1].right):
                    curr=s.pop()
                    print(curr.val)
                    ''' the below condition is for while condition not throw exception when stack is empty s[len(s)-1].right  '''
                    if len(s)==0:
                        break
            if len(s)!=0:
                curr=s[len(s)-1].right
            else:
                curr=None
                    

# arr=[25,19,23,15,40,39,45]
# root=None
# root=buildBST_Iterative_withNoStack(root,arr)
# PostorderTraversalRecursion(root)    
# print("=========================")
# PostorderTraversal_Iterative(root)
# print("=========================")
