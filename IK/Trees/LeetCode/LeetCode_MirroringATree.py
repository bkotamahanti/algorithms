'''
Created on Jul 15, 2019

@author: bkotamahanti
'''
# from IK.Trees.common.NodeFile import Node
from binarytree import Node
''' 
Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1




'''

class Solution:
    ''' swapping with siblings before going to its kids '''
    def invertTreeRecursion1(self, root: Node) -> Node:
        if root==None:
            return
        if root.left==None and root.right==None:
            return root
        root.left,root.right=root.right,root.left
        self.invertTreeRecursion1(root.left)
        self.invertTreeRecursion1(root.right)
        return root
    
    ''' swapping kids first and then swapping with  siblings '''
    def invertTreeRecursion2(self, root: Node) -> Node:
        if root==None:
            return
        if root.left==None and root.right==None:
            return root
        self.invertTreeRecursion2(root.left)
        self.invertTreeRecursion2(root.right)
        root.left,root.right=root.right,root.left
        return root
    '''Iterative approach using DFS ''' 
    def invertTreeItr(self, root: Node)->Node:
        if root==None:
            return
        curr=root 
        s=[]
        while(len(s)!=0 or curr!=None):
            if curr!=None:
                if curr.left==None and curr.right==None:
                    # if len(s)!=0:
                    #     curr=s.pop()
                    #     curr=curr.right
                    #     continue
                    curr=None
                else:
                    s.append(curr)
                    curr.left,curr.right=curr.right,curr.left
                    curr=curr.left
            else:
                if len(s)!=0:
                    curr=s.pop()
                    curr=curr.right
         
        return root
    
    ''' Iterative approach using BFS'''
   
#  

'''Constructing a binary tree '''
root=Node(4)
root.left=Node(2)
root.right=Node(7)
root.left.left=Node(1)
root.left.right=Node(3)
root.right.left=Node(6)
root.right.right=Node(9)
print(root)
# print(root.size)
# print(root.leaf_count)
# print("++++++++++++")
# print(root.leaves)
# print(root.levels)
sol=Solution()
root=sol.invertTreeRecursion1(root)
print(root)
root=sol.invertTreeRecursion2(root)
print(root)
root=sol.invertTreeItr(root)
print(root)
        
        