'''
Created on Jul 14, 2019

@author: bkotamahanti
'''
from typing import List

from IK.Trees.DFS.BST_InorderTraversal import InorderTraversalRecursion


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
        if len(nums)==0:
            return None
        def recursionBST(start, end, nums):
            if start>end:
                return None
            if start==end:
                return TreeNode(nums[start])
            
            import math
            mid=math.ceil((start+end)/2)

            root=TreeNode(nums[mid])
            root.left=recursionBST(start,mid-1,nums)
            root.right=recursionBST(mid+1,end, nums)
            return root
        return recursionBST(0, len(nums)-1, nums)
    
arr=[-10,-3,0,5,9]
sol=Solution()
root=sol.sortedArrayToBST(arr)
InorderTraversalRecursion(root)
