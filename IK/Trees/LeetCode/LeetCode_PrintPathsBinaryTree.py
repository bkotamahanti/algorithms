'''
Created on Oct 31, 2019

@author: bkotamahanti
'''

from typing import List
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None



class Solution:
    def binaryTreePaths(self, root: TreeNode) -> List[str]:
        def allPathsRec(root, temp_path, paths):
            if root==None:
                return

            temp_path.append(root.val)
            if root.left==None and root.right == None:
                temp_path_str= "->".join(str(v) for v in temp_path)
                paths.append(temp_path_str)
                temp_path.pop()
                return

            allPathsRec(root.left, temp_path, paths) 
            allPathsRec(root.right, temp_path, paths)
            temp_path.pop()

        paths=[]
        temp_path=[]
        allPathsRec(root, temp_path, paths)
        return paths 


root=TreeNode(4)
root.left=TreeNode(2)
root.right=TreeNode(7)
root.left.left=TreeNode(1)
root.left.right=TreeNode(3)
root.right.left=TreeNode(6)
root.right.right=TreeNode(9)
print(root) 

sol=Solution()
print(sol.binaryTreePaths(root))