'''
Created on Oct 10, 2019

@author: bkotamahanti
'''
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        
class Solution:
    '''
    Time:O(n)
    Space:O(2**(h-1))
    we can do iterative approach either using DFS stack or BFS queue and add leaf nodes using flag
    '''
    def sumOfRightLeaves(self, root: TreeNode) -> int:
        total=0
        from collections import deque 
#         stack=deque([])
        stack=[]
        flag=False
        if root == None:
            return total
        stack.append((root,False))
        while(stack):
            curr, flag=stack.pop()
            if curr.right==None and curr.left==None and flag==True:
                total+=curr.val
            if curr.right!= None:
                stack.append((curr.right,True))
            if curr.left!= None:
                stack.append((curr.left,False))          
        return total
root=TreeNode(3)
root.left=TreeNode(9)
root.left.left=TreeNode(6)
root.left.right=TreeNode(7)
root.right=TreeNode(20)
sol=Solution()
print(sol.sumOfRightLeaves(root))
