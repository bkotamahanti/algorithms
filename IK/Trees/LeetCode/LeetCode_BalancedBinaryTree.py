'''
Created on Nov 16, 2019

@author: bkotamahanti
'''
# Definition for a binary tree node.

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    flag=True
    def isBalanced(self, root: TreeNode) -> bool:
        self.balancedHelper(root, 0, 0)
        return self.flag
    
    def balancedHelper(self, node, Lheight, Rheight):
        if node==None:
            return 0
        Lheight=self.balancedHelper(node.left, Lheight, Rheight)
        Rheight=self.balancedHelper(node.right, Lheight, Rheight)
        
        if not abs(Lheight-Rheight)<=1:
            Solution.flag=False
        
        return 1+max(Lheight, Rheight)
        
        
# root=None

# [3,9,20,null,null,15,7]
root=TreeNode(3)
root.left=TreeNode(9)
root.right=TreeNode(20)
root.left.left=None
root.left.right=None
root.right.left=TreeNode(15)
root.right.right=TreeNode(7)
root.right.right.left=TreeNode(6)
root.right.right.right=TreeNode(34)
sol=Solution()
print(sol.isBalanced(root))