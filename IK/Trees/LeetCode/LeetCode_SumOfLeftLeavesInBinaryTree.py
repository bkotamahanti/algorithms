'''
Created on Oct 10, 2019

@author: bkotamahanti
'''
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    '''
    Time:O(n)
    Space:O(2**(h-1))
    we can do iterative approach either using DFS stack or BFS queue and add leaf nodes using flag
    '''
    def sumOfLeftLeaves2(self, root: TreeNode) -> int:
        total=0
        from collections import deque 
        stack=deque([])
        # stack=[]
        flag=False
        if root == None:
            return total
        stack.append((root,False))
        while(stack):
            curr, flag=stack.pop()
            if curr.right==None and curr.left==None and flag==True:
                total+=curr.val
            if curr.right!= None:
                stack.append((curr.right,False))
            if curr.left!= None:
                stack.append((curr.left,True))          
        return total
    
    ''' 
    Space:O(logN) in balanced tree in worst case O(n)
    Time: O(n)
    '''
    def sumOfLeftLeaves3(self, root: TreeNode) -> int:
        total=0
        return self.rec(root, total, False)
    
    def rec(self, root, total, left):
        if root==None:
            return total
        if root.left==None and root.right==None and left==True:
            total+=root.val
            return total
        total=self.rec(root.left, total, True)
        total=self.rec(root.right, total, False)
        return total
    
    
    def sumOfLeftLeaves(self, root: TreeNode) -> int:
        curr=root
        total=0
        stack=[]
        flag=False
        '''using Inorder traversal'''
        while(curr or stack):
            while (curr):
                stack.append((curr,flag))
                curr=curr.left
                flag=True
            curr, flag=stack.pop()
            if curr.right==None and curr.left==None and flag==True:
                total+=curr.val
            
            curr=curr.right
            flag=False
            
        return total
            