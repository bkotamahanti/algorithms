'''
Created on Nov 25, 2019
https://leetcode.com/problems/binary-tree-level-order-traversal/
@author: bkotamahanti
'''

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
from typing import List
from collections import deque
class Solution:
    def levelOrder_finding_numberOfNodesAtEachLevelInQueue(self, root: TreeNode) -> List[List[int]]:
        if root==None:
            return []
        
        output=[]
        temp_l=[]
        q=deque([root])
       
        while(len(q)!=0):
            numofnodes=len(q)
            '''finding number of nodes at each level before poping it out'''
            for _ in range(numofnodes):
                node=q.popleft()
            
                if node.left!=None:
                    q.append(node.left)
                if node.right!=None:
                    q.append(node.right)
                
                temp_l.append(node.val)

            output.append(temp_l)
            temp_l=[]
            
        return output
    
    
    '''aproach one :using level marker and temp_list extra space
        Time:O(n)
        Space:O(n)
    '''
    def levelOrder_level_marker(self, root: TreeNode) -> List[List[int]]:
        def processTempList():
            output.append(temp_l[:])
            temp_l.clear()
            
        
        if root==None:
            return []
        output=[]
        temp_l=[]
        prev_level=0
        q=deque([(root,0)])
        
        while(len(q)!=0):
            node, level=q.popleft()
            
            if prev_level!=level:
                processTempList()
                prev_level=level
            
            if node.left!=None:
                q.append((node.left,level+1))
            if node.right!=None:
                q.append((node.right, level+1))
            
            temp_l.append(node.val)
        if temp_l:
            output.append(temp_l)
        return output
        
    
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        output=[]
        q=deque()
        if root is not None: 
            q.append(root)
            i=0
            j=1
        temp_l=[]   
        while(q):
            node=q.popleft()
            i+=1
            if node.left!=None:
                q.append(node.left)
            if node.right!=None:
                q.append(node.right)
            
            temp_l.append(node.val)
            if i==j:
                output.append(temp_l)
                temp_l=[]
                i=0
                j=len(q)
            
                
        
        return output
        
    
    
    
