'''
Created on Oct 11, 2019

@author: bkotamahanti
'''
class TreeNode():
    def __init__(self, x):
        self.val=x
        self.left=None
        self.right=None

class Solution():
    total=0
    def __init__(self):
        pass
    def sumOfLeaves(self, root):
        self.sumOfLeavesRec(root)
        return Solution.total
    
    def sumOfLeavesRec(self, root):
        if root==None:
            return 
        if root.left==None and root.right==None:
            Solution.total+=root.val
        self.sumOfLeaves(root.left)
        self.sumOfLeaves(root.right)
        
root=TreeNode(3)
root.left=TreeNode(9)
root.right=TreeNode(20)
root.left.left=TreeNode(6)
root.left.right=TreeNode(7)
sol=Solution()
sol2=Solution()
print(sol.sumOfLeaves(root))
print(sol2.sumOfLeaves(root))
