# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    dept_x=dept_y=-1
    parent_x=parent_y=None
    
    '''Recursion:Using one pass traversal in tree rather than 2 pass traversal tree'''
    def isCousins(self, root: TreeNode, x: int, y: int) -> bool:
        Solution.dept_x=Solution.dept_y=-1
        Solution.parent_x=Solution.parent_y=None
    
        if root==None:
            return False
        '''O(n)'''
        self.depthAndParentOfXY(root, None, x, y, 0)
        #print(Solution.dept_x, Solution.dept_y)
        #print(Solution.parent_x, Solution.parent_y)
        
        if Solution.dept_x!=Solution.dept_y:
            return False
        elif Solution.parent_x!=Solution.parent_y:
            return True
        else:
            return False
    
    
    def depthAndParentOfXY(self, node, parent, x, y, dept):
        if node==None:
            return 
        if Solution.dept_x != -1 and Solution.dept_y!=-1:
            return
        if node.val==x:
            Solution.dept_x=dept
            Solution.parent_x=parent
        if node.val==y:
            Solution.dept_y=dept
            Solution.parent_y=parent
        
        self.depthAndParentOfXY(node.left, node, x, y, dept+1) 
        self.depthAndParentOfXY(node.right, node, x, y, dept+1) 
    
    '''Recursion: Using 2 pass traversal in binary tree'''        
    def isCousins2(self, root: TreeNode, x: int, y: int) -> bool:
        if root==None:
            return False
        dept=0
        parent=None
        '''O(n)'''
        dep_x, parent_x=self.depthAndParentOf(root, parent, x, dept)
        '''O(n)'''
        dep_y, parent_y=self.depthAndParentOf(root, parent, y, dept)
        # print(dep_x, dep_y)
        # print(parent_x,parent_y)
        if dep_x!=dep_y:
            return False
        elif parent_x!=parent_y:
            return True
        else:
            return False
        
        
    def depthAndParentOf(self, curr, parent, x, dept):
        if curr==None:
            return -1, parent
        if x==curr.val:
            return dept, parent
        res_dept, parent=self.depthAndParentOf(curr.left, curr, x, dept+1)
        if res_dept==-1:
            res_dept, parent=self.depthAndParentOf(curr.right, curr, x, dept+1)
        return res_dept, parent
        
    def isCousins1(self, root: TreeNode, x: int, y: int) -> bool:
        if root==None:
            return False
        curr=root
        from collections import deque
        q=deque([(root,0),(root,0)])
        while(q ):
            node1,node1_dep=q.pop()
            node2,node2_dep=q.pop()
            if (not node1) or (not node2):
                pass
            if (node1.left==x and node2.right==y) or (node1.left==y and node1.right==x):
                return False
            else:
                node1.left=node1.left
                node1.right=node1.right
                node2.left=node2.right
                # if node1.left.left
            q.append(node1.left,node1_dep+1)
            q.append(node2.right,node2_dep+1)
            
            
    