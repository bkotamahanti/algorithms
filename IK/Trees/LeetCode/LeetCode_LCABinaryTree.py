'''
Created on Oct 29, 2019

@author: bkotamahanti
'''

from binarytree import Node
ans=None
def lca(root, a, b):
    global ans
    ans=None
    def recursion(curr):
        if curr==None:
            return False
        left=recursion(curr.left)
        right=recursion(curr.right)
        mid= curr.value == a or curr.value==b
        if mid + left + right >= 2:
            global ans
            ans=curr
        return mid or left or right
    
    recursion(root)
    return ans

root=Node(1)
root.left=Node(2)
root.right=Node(3)
root.left.left=Node(4)
root.left.right=Node(5)
root.right.left=Node(6)
root.right.right=Node(7)
root.left.right.left=Node(8)
root.left.right.right=Node(9)

print(root)
print(lca(root, 8,9).value)