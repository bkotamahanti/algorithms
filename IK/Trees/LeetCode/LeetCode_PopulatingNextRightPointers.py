'''
Created on Nov 26, 2019

@author: bkotamahanti
https://leetcode.com/problems/populating-next-right-pointers-in-each-node/
'''
# Definition for a Node.
class Node:
    def __init__(self, val, left, right, next):
        self.val = val
        self.left = left
        self.right = right
        self.next = next

import collections
class Solution:
    def connect(self, root : 'Node') -> 'Node':
        queue=collections.deque()
        nq=collections.deque()
        if root is not None:
            queue.append(root)
        
        while queue:
            while queue:
                node=queue.popleft()
                if len(queue)!=0:
                    node.next=queue[0]
                
                if node.left:
                    nq.append(node.left)
                if node.right:
                    nq.append(node.right)
            queue=nq
            nq=collections.deque()
            
        return root
    
    def connect_using_level_boundary(self, root: 'Node') -> 'Node':
        queue=collections.deque()
        if root is not None:
            queue.append(root)
        
        while(queue):
            numofnodes=len(queue)
            i=0
            for i in range(numofnodes):
                node=queue.popleft()
                if numofnodes==i+1:
                    pass
                else:
                    node.next=queue[0]
                
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
        
        return root
    
    
    def connect_using_level_marker(self, root: 'Node') -> 'Node':
        queue=collections.deque()
        if root is not None:
            queue.append((root,0))
        
        while(queue):
            node, level = queue.popleft()
            # print(" {},{}".format(node.val, level))
            if len(queue)!=0:
                node_t, level_t= queue[0]
                # print("node_t {} level_t {}".format(node_t.val, level_t))
                if level_t==level:
                    node.next=node_t
        
            if node.left:
                queue.append((node.left, level+1))
            if node.right:
                queue.append((node.right, level+1))
    
    
        return root 
            
            
                
