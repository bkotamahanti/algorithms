'''
Created on Aug 13, 2019

@author: bkotamahanti

'''
# from binarytree import Node

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    ''' The below sol dont work. as it will consider only root val and check if k-root.val exist in right or left subtree but we 
    need to compare all elements of BST tree  say ex [5,4] [2,7] [3,6] are valid solutions
    def findTarget(self, root: TreeNode, k: int) -> bool:
        if not root:
            return False
        exp_val=k-root.val
        while(root):
            if exp_val<=root.val:
                root=root.left
            else:
                root=root.right
            if root and root.val==exp_val:
                return True
        
        return False
    '''
    '''Sol1: Using Hashset preorder DFS approach 
    Time:O(n)
    Space:O(n)
    def findTarget(self, root, k: int) -> bool:
#         do inorder traversal and build set while traversing and check if diff is present in set or not
        s=set()
        return self.preorder(root, k, s)
    
    
    def preorder(self,root, k, s):
        if root==None:
            return False
        if k-root.val in s:
            return True
        s.add(root.val)
        # flag=self.inorder(root.left,k,s)
        # if flag==True:
        #     return True
        # flag=self.inorder(root.right,k,s)
        # return flag
        return self.preorder(root.left, k, s) or self.preorder(root.right, k, s)
    '''
    

    
    ''' Sol2:
    Inorder DFS Hashset
    Time:O(n)
    Space:O(n)
    
    
    def findTarget(self, root: TreeNode, k: int) -> bool:
        do inorder traversal and build set while traversing and check if diff is present in set or not
        s=set()
        return self.inorder(root, k, s)
        
    def inorder(self,root, k, s):
        left root right so we travel first left most and then root and then right using DFS. but here we are adding elements only after touching NULL node. it is efficient but impede process of adding elements till we reach left most. Instead add elements as soon as element is 
        found. I understand in Inorder we traverse first left root right if they ask to print nodes which prints in ascending order. Here set.add() stmt is nothing but print stmt in normal inorder traversal. 
        if root==None:
            return False
        flag=self.inorder(root.left,k,s)
        if flag==True:
            return True
        if k-root.val in s:
            return True
        s.add(root.val)
        flag=self.inorder(root.right,k,s)
        return flag
        
     '''  
    '''
        Sol:3 Inorder traversal add to List and two pointer approach
        Time:O(n)+O(n)
        Space: O(n)
    
    public class Solution {
    public boolean findTarget(TreeNode root, int k) {
        List < Integer > list = new ArrayList();
        inorder(root, list);
        int l = 0, r = list.size() - 1;
        while (l < r) {
            int sum = list.get(l) + list.get(r);
            if (sum == k)
                return true;
            if (sum < k)
                l++;
            else
                r--;
        }
        return false;
    }
    public void inorder(TreeNode root, List < Integer > list) {
        if (root == null)
            return;
        inorder(root.left, list);
        list.add(root.val);
        inorder(root.right, list);
    }
}

    '''
    def findTarget(self, root: TreeNode, k: int) -> bool:
        s=set()
        import queue
        q=queue.Queue(0)
        q.put(root)
        while(not q.empty()):
            node=q.get()
            if not (node==None) and not (node=='null'):
                if k-node.val in s:
                    return True
                s.add(node.val)
                q.put(node.right)
                q.put(node.left)
            else:
                q.get()
        
        return False
                
   
root=TreeNode(4)
root.left=TreeNode(2)
root.right=TreeNode(7)
root.left.left=TreeNode(1)
root.left.right=TreeNode(3)
root.right.left=TreeNode(6)
root.right.right=TreeNode(9)
print(root) 

sol=Solution()
print(sol.findTarget(root, 7))