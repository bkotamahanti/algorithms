'''
Created on Oct 7, 2019

@author: bkotamahanti
'''

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        isSym=True
        if root==None:
            return isSym
        curr=root
        from collections import deque
        q=deque([curr])
        while (q and isSym):
            curr_len=len(q)
            k=0
            while(k<curr_len):
                curr=q.popleft()
                if curr!=None and (curr.left or curr.right):
                    q.append(curr.left)
                    q.append(curr.right)
                k+=1
                
            i=0
            j=len(q)-1
            while(i<j):
                if (q[i]==None and q[j]==None) or (q[i].val==q[j].val):
                    i+=1
                    j-=1
                elif( ( q[i]==None and q[j]!=None) or ( q[i]!=None and q[j]==None) or (q[i].val!=q[j].val)):
                    isSym=False
                    break
            # if not isSym:
            #     break
        return isSym
            
sol=Solution()
root=TreeNode(1)
root.left=TreeNode(2)
root.right=TreeNode(2)
root.left.left=TreeNode(3)
root.left.right=TreeNode(4)
root.right.left=TreeNode(4)
root.right.right=TreeNode(3)
print(sol.isSymmetric(root))       