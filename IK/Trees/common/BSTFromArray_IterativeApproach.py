'''
Created on Jun 26, 2019

@author: bkotamahanti
'''
''' this function exactly mimics the recursive approach in iterative fashion. But in reality we dont need to maintain the stack
of references as we just need to insert a node in BST. Once the node connection is established b/w parent and child we dont 
need to maintain the chain of all parent references. Thats way it is sufficient to remember the prev parent node rather than 
creating a stack of parent ref's. which is explained next method'''

from IK.Trees.common.NodeFile import Node


def buildBST_Iterative_using_Stack(root, arr):
    for i in range(len(arr)):
        s=[]
        if not root:
            root=Node(arr[i])
            ''' to make uniform code with array of 1 element or array of multiple elements set current = root and return current at end of for loop'''
            current=root
            continue
        
        while(current):
            if arr[i] <= current.val:
                s.append((current,'L'))
                current=current.left
            else:
                s.append((current,'R'))
                current=current.right        
        current=Node(arr[i])
        while(s):
            parent,subtree=s.pop()
            if subtree=='L':
                parent.left=current
            else:
                parent.right=current
            current=parent
    ''' at the end of execution here current will be root at any cost when we pop stack root is last element to pop.'''
    return current


''' the below code just remembers the prev node until it finds current==NULL'''
def buildBST_Iterative_withNoStack(root, arr):
    for i in range(len(arr)):
        if not root:
            root=Node(arr[i])
            ''' to make uniform code with array of 1 element or array of multiple elements set current = root and return current at end of for loop'''
            continue
        
        current=root
        while(current):
            prev=current
            if arr[i] <= current.val:
                current=current.left
            else:
                current=current.right       
        current=Node(arr[i])
        if arr[i] <= prev.val:
            prev.left=current
        else:
            prev.right=current
            
        ''' at the end of execution here current will be root at any cost when we pop stack root is last element to pop.'''
    return root

# arr=[20,17,40,15,19,35,45]
# root=None
# root=buildBST_Iterative_using_Stack(root, arr)
# printBST(root)
# print("=======================")
# root=None
# root=buildBST_Iterative_withNoStack(root, arr)
# printBST(root)


