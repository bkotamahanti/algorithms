'''
Created on May 30, 2019

@author: bkotamahanti
'''
from IK.Trees.common.NodeFile import Node

def constructBSTFromArray(arr, root):
    i=0;
    n=len(arr)
#     global root
    ''' Need to traverse all the elements of array so Time:O(n)
        Constructing a tree:  for inserting 1 element it takes logN as we cut down at each step n/2 elements but if we want to insert
        n elements nlogn . Time=nlogn Space: logN stack space and O(n) for array ., O(n+logn)
    '''
    while(i<n):
        root=recursionBuildBST(arr[i],root)
        i=i+1
    return root
def recursionBuildBST(val, current):
#     nonlocal root
#     current=root
    if current==None:
        current=Node(val)
        return current
    if val<=current.val:
        current.left=recursionBuildBST(val, current.left)
    else:
        current.right=recursionBuildBST(val,current.right)
    return current

def printBST(root):
    inorderDFS(root)
''' 
in case of balanced tree or unbalanced tree Time=O(n) since you need to visist each node
in balanced tree Space=O(logn) for tree traversal but in case of skewed tree Space=O(n)
we always consider worst case complexity rather than best case unless someone asks what is best case of algorithm
'''
def inorderDFS(current):
    if current==None:
        return
    inorderDFS(current.left)
    print(current.val)
    inorderDFS(current.right)
    
# root=None
# arr=[12,10, 8, 11, 56,23,67,90]
# root=constructBSTFromArray(arr,root)
# printBST(root)
# findInorderSuccessorOfNode(11, root)