'''
Created on Jul 19, 2019

@author: bkotamahanti
'''
from binarytree import bst, Node
# Definition for a binary tree node.
# class Node:
#     def __init__(self, x):
#         self.value = x
#         self.left = None
#         self.right = None


''' Time Worst case : O(n) Skewed tree case 
    Best case: O(logn) ordered tree, balanced tree
    Space: Best case height of tree which is O(logN) for balanced
    Worst case: height of tree to leaf node in case of skewed tree it is O(N)'''
        
def insertIntoBST(root: Node, val: int) -> Node:
    ''' you dont need below stmts. if we consider them then in caller function you need to root.left = insertIntoBST() this will go
    from bottom of tree to top of tree by assigning back root to prev root val which is not required. For establishing connection
    bw parent and child we can do as below code in if else condition where it establishes connection there and then if left or right is None '''
#     if root==None:
#         return Node(val)
    if val < root.value:
        if (root.left == None): 
            root.left=Node(val)
            return
        insertIntoBST(root.left, val)
    else:
        if (root.right == None):
            root.right=Node(val);
            return
        insertIntoBST(root.right, val)
    

def insertToBST(root: Node, val: int) -> Node:
    if (root == None):
        return Node(val)
    insertIntoBST(root, val)


def insertIntoBSTItr(root: Node, val: int) -> Node:
    if root==None:
        return Node(val)
    curr=root
    while(curr!=None):
        prev=curr
        if val < curr.value:
            curr=curr.left
            if curr==None:
                prev.left=Node(val)
        else:
            curr=curr.right
            if curr==None:
                prev.right=Node(val)
#         if val<prev.val:
#             prev.left=TreeNode(val)
#         else:
#             prev.right=TreeNode(val)
        
    return root
            
'''if root is None that means tree is empty and lets say if they ask to insert a node to BST either using array or Linkedlist'''

root=None
root=insertToBST(root, 7)
print(root)

insertToBST(root, 8)
print(root)

insertToBST(root, 6)
print(root)
print("==================================")

'''If BST is already given '''
root=bst(is_perfect=True, height=2)
print(root)
insertIntoBST(root, 7)
print(root)
insertIntoBSTItr(root, -1)
print(root)
        
    
        





