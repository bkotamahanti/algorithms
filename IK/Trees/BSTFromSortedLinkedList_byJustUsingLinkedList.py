'''
Created on Jun 28, 2019

@author: bkotamahanti
'''
# Definition for singly-linked list.
from IK.Trees.BSTFromArray_RecursionApproach import printBST
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode():
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution():
    def createLinkedListFromArray(self, arr):
        head=tail=None
        for i in range(len(arr)):
            if head==None:
                head=ListNode(arr[i])
                tail=head
                continue
            tail.next=ListNode(arr[i])
            tail=tail.next
        return head

    def sortedListToBST(self, head):
            """
            :type head: ListNode
            :rtype: TreeNode
            """
            if not head :
                return None
            if not head.next:
                return TreeNode(head.val)
            slow=fast=head
            prev=None
            while(fast and fast.next):
                fast=fast.next.next
                prev=slow
                slow=slow.next
            
            prev.next=None
            root=TreeNode(slow.val)
            root.left=self.sortedListToBST(head)
            root.right=self.sortedListToBST(slow.next)
            
            return root


m=Solution()
arr=[-10,-3,0,5,9]
arr=[4,8,9,12,15,19,22,25]
head=m.createLinkedListFromArray(arr)
root=m.sortedListToBST(head)
print("====================")
printBST(root)