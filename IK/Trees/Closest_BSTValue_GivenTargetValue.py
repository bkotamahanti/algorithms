'''
Created on Jul 1, 2019

@author: bkotamahanti
'''
from IK.Trees.BSTFromArray_IterativeApproach import buildBST_Iterative_using_Stack
from IK.Trees.BSTFromArray_RecursionApproach import printBST

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    
    def closestValue(self, root: TreeNode, target: float) -> int:
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        
        """
        result=root.val
        while root:
            if abs(root.val-target) < abs(result-target):
                result=root.val
            elif target<root.val:
                root=root.left
            else:
                root=root.right
        return result
sol=Solution()
arr=[4,2,5,1,3]
arr=[20,17,40,15.1,15.9,19,35,45]
root=None
print("++++++++++++++++++++++++")
root=buildBST_Iterative_using_Stack(root,arr)
printBST(root)
print("++++++++++++++++++++++++")
print(sol.closestValue(root, 15.6))