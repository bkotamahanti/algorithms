'''
Created on Sep 19, 2019

@author: bkotamahanti
https://leetcode.com/problems/reverse-integer/

Given a 32-bit signed integer, reverse digits of an integer.
'''
class Solution:
    ''' 
    Time: O(log(x) base 10)
    Space: O(1)
    
    '''
    def reverse(self, x: int) -> int:
        flagNeg=1
        if x<0:
            flagNeg=-1
            x=x*flagNeg
        rev=0
        while(x>0):
            rem=x%10
            rev=rev*10+rem
            x=x//10
            
        
        if -2**31 <= rev*flagNeg <=2**31-1:
            return rev*flagNeg
        else:
            return 0
sol=Solution()
print(sol.reverse(123))
