'''
Created on Sep 25, 2019

@author: bkotamahanti
'''
import sys
class MinStack:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.minsta=[]
        self.ind=-1
        self.minele=sys.maxsize

    def push(self, x: int) -> None:
        self.minsta.append(x)
        self.ind+=1
        self.minele= min(self.minsta)

    def pop(self) -> None:
        if self.ind!=-1:
            self.ind-=1
        if self.ind!=-1:
            self.minele= min(self.minsta[0:self.ind])
        else:
            self.minele=0

    def top(self) -> int:
        return self.minsta[self.ind]

    def getMin(self) -> int:
        print("getmin func")
        return self.minele
    
    

# Your MinStack object will be instantiated and called as such:
obj = MinStack()
obj.push(-2)
obj.push(0)
obj.push(-3)
print(obj.top())
obj.pop()
obj.pop()
obj.pop()
print(obj.getMin())