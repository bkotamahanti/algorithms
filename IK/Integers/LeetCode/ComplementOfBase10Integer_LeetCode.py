'''
Created on Sep 20, 2019

@author: bkotamahanti

Every non-negative integer N has a binary representation.  For example, 5 can be represented as "101" in binary, 11 as "1011" in binary, and so on.  Note that except for N = 0, there are no leading zeroes in any binary representation.

The complement of a binary representation is the number in binary you get when changing every 1 to a 0 and 0 to a 1.  For example, the complement of "101" in binary is "010" in binary.

For a given number N in base-10, return the complement of it's binary representation as a base-10 integer.

https://leetcode.com/problems/complement-of-base-10-integer/
'''
class Solution:
    def bitwiseComplement(self, N:int) -> int:
        if N==0:
            return 1
        res=0
        pos=0
        '''converting decimal to binary #'''
        while(N>0):
            rem=N%2
            res=res+rem*10**pos
            N//=2
            pos+=1
        print(res)
        pos=0
        result=0
        '''complementing res'''
        while(res>0):
            rem=res%10
            result=result+(0 if rem==1 else 1)*10**pos
            res//=10
            pos+=1
        print(result)
        res=0
        pos=0
        
        '''converting binary to decimal'''
        while(result>0):
            rem=result%10
            res=res+rem*2**pos
            result//=10
            pos+=1
        return res
    
    ''' 
    Time:O(sizeOf(N)) O(logN base 2 + 1)
    Space:O(1)
    '''
    def bitwiseComplement1(self, N: int) -> int:
        if N==0:
            return 1
        pos=0
        result=0
        while(N>0):
            rem=N%2 # the numbers divisible by 2 leave 0 or 1 as rem's
#           our intention is not to find the binary number but and find complement and then find decimal out of binary
#           as it is long process. we just need  final decimal number
            result=result+(0 if rem==1 else 1)*2**pos
#             N//=2
            N=N>>1
            pos+=1
            
        print(result)
        return result
    
    




def bitwiseComplement(N):
        if(N == 0):
            return 1;
        res = 0;
        k = 1;
        while(N > 0):
            bit = N%2
            res += (0 if bit == 1 else  1)*k
            k *= 2;
            
            N //= 2;
        
        return res;
    
def bitwiseComplement1(N):
    res=0
    pos=0
    while(N>0):
        rem=N%2
        res= res + (0 if rem == 1 else  1)*10**pos
        N//=2
        pos+=1
    pos=0
    result=0
    while(res>0):
        rem=res%10
        result=result+(rem*2**pos)
        pos+=1
        res//=10
    
    return result
def bitwiseComplement2(N):
    res=0
    pos=0
    while(N>0):
        rem=N%2
        res= res + (0 if rem == 1 else  1)*2**pos
        N = N>>1
        pos+=1
#     pos=0
#     result=0
#     while(res>0):
#         rem=res%10
#         result=result+(rem*2**pos)
#         pos+=1
#         res//=10
#     
    return res
print(bitwiseComplement2(5))
