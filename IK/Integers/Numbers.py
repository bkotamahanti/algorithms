'''
Created on Aug 23, 2019

@author: bkotamahanti
'''
class Solution:
    ''' converting to string O(n) space O(n) Time '''
    def isPalindrome1(self, x: int) -> bool:
        if x<0:
            return False
        s=str(x)
        i=0
        j=len(s)-1
        while(i<j):
            if s[i]!=s[j]:
                return False
            i +=1
            j -=1
        
        return True
    ''' reverse number and compare 2 numbers time:O(n) space :O(1) but it can have Max int overflow'''
    def isPalindrome2(self, x: int)-> bool:
        if x<0:
            return False
        if x==0:
            return True
        rev_no=0
        x1=int(x)
        while(x1!=0):
            rev_no= 10 * rev_no + x1%10
            x1 //= 10
        
        return x==rev_no
    ''' reversing half of the number and comparing with remaining x value. Consider odd and even number at the end. 121 rev_no will have 12 x will have remaining 1. but u need to ignore 2 in rev_num here.'''
    def isPalindrome(self, x: int)-> bool:
        if (x<0 or (x % 10 == 0 and x!=0)):
            return False
        if x==0:
            return True
        rev_no=0
        
        while(x>rev_no):
            rev_no= 10 * rev_no + x%10
            x //= 10
        return x == rev_no or x == rev_no//10
        
sol=Solution()
print(sol.isPalindrome(121))