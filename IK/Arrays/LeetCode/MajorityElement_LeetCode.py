'''
Created on Aug 12, 2019

@author: bkotamahanti

https://leetcode.com/problems/majority-element/
Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.

'''
from typing import List
import collections


class Solution():
    '''brute force 
       Time: O(n**2)
       Space: O(1)
    def majorityElement(self, nums: List[int]) -> int:
        maj_element=len(nums)//2
        for num in nums:
            count=0
            count = sum(1 for elem in nums if elem==num)
            if count>maj_element:
                return num
    
    '''
    ''' 
        Approach 2: HashMap
        Time:O(n)
        Space:O(n)
    
    def majorityElement(self, nums: List[int]) -> int:
        majority_count=len(nums)//2
        d={}
        for i in range(len(nums)):
            if nums[i] not in d:
                d[nums[i]]=1
            else:
                d[nums[i]]=d[nums[i]]+1
                 
            if d[nums[i]]>majority_count:
                return nums[i]
        return -1
    
    def majorityElement(self, nums):
        counts = collections.Counter(nums)
        print(counts)
        return max(counts.keys(), key=counts.get)
    '''
    
    '''Using Sorting technique 
    Time: O(nlogn)
    Space:O(1)
    
    
    def majorityElement(self, nums: List[int])->int:
        nums.sort();
        return nums[len(nums)//2];
    
    
    '''
           
           
    '''Boyer-Moore Voting Algorithm
    Time: O(n)
    Space:O(1)
     '''           
    def majorityElement(self, nums: List[int])->int:
        candidate=None
        count=0
        for num in nums:
            if count==0:
                candidate=num
            if candidate==num:
                count=count+1
            else:
                count=count-1
        
        if count!=0:
            count=0
            for num in nums:
                if candidate==num:
                    count=count+1
            if count>len(nums)//2:
                return candidate
        else:
            return -1
    

sol=Solution()
print(sol.majorityElement([2,2,1,1,1,2,3]))
        