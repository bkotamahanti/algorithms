'''
Created on Jul 22, 2019

@author: bkotamahanti
'''
from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        '''
            Understand the problem
            with different inputs and observe for patterns
            [7,1,5,3,6,4]
           [7,6,4,3,1]
           [7,5,10,1,3,4]
           []
           [7]
        
        '''
        # if len(prices)==0 or len(prices)==1:
        #     return 0
        
        max_price=0
        max_profit=0
        import sys
        min_price=sys.maxsize
        for i in range(0,len(prices)):
            if prices[i] < min_price:
                min_price=prices[i]
                max_price=0
                continue
            if prices[i] > max_price:
                max_price=prices[i]
                if max_profit < (max_price-min_price):
                    max_profit=max_price-min_price
                    
        
        return max_profit
                
        
arr=[7,1,5,3,6,4] 
arr=[7,6,4,3,1]   
arr=[7,8,5,15,3,1,9]           
sol=Solution()
print(sol.maxProfit(arr))
