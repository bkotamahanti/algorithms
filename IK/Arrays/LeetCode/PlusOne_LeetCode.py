'''
Created on Aug 16, 2019

@author: bkotamahanti

https://leetcode.com/problems/plus-one/


Given a non-empty array of digits representing a non-negative integer, plus one to the integer.

The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.

You may assume the integer does not contain any leading zero, except the number 0 itself.

Example 1:

Input: [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
'''
from typing import List

class Solution:
    ''' Time:O(3n) 
        Space:O(1)
    '''
    def plusOne(self, digits: List[int]) -> List[int]:
        ''' O(n)'''
        for i in range(len(digits)//2):
            digits[i],digits[len(digits)-1-i]=digits[len(digits)-1-i],digits[i]
        print(digits)
        carry=0
        i=0
        increment=1
        '''O(n)'''
        while(i<len(digits)):
            if i!=0:
                increment=0
            x=digits[i]+increment
            digits[i]=(x+carry)%10
            carry=(x+carry)//10
            i=i+1
        if carry:
            digits.append(carry)
        '''O(n)'''
        digits.reverse()
        print(digits)
#         for i in range(len(digits)//2):
#             digits[i],digits[len(digits)-1-i]=digits[len(digits)-1-i],digits[i]
         
        return digits
            
    '''Time: O(n)
    '''
    def plusOne1(self, digits: List[int]) -> List[int]:
        carry=1
        for i in range(len(digits)-1,-1,-1):
            x=digits[i]+carry
            carry, digits[i]=divmod(x,10)
        '''worst case shift to right O(n) '''
        if carry:
            digits.insert(i,carry)
        
        return digits
            
        
            