'''
Created on Aug 13, 2019

@author: bkotamahanti
https://leetcode.com/problems/two-sum/
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
'''
from typing import List

class Solution:
    ''' using dictionary 
        Space:O(n)
        Time:O(n)
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        d={}
        for i in range(len(nums)):
            if target-nums[i] in d:
                return d[target-nums[i]],i
            d[nums[i]]=i
     '''
#      brute force approach is O(n2) O(1) space
# below sol is O(n) both time and space
#     def twoSum(self, nums: List[int], target:int )->List[int]:
#         d={}
#         for i in range(len(nums)):
#             d[nums[i]] = i
#         
#         for i in range(len(nums)):
#             if target-nums[i] in d and d[target-nums[i]]!=i:
#                 return [ i,d[target-nums[i]]]
#         
        