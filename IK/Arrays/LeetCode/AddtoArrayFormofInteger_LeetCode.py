'''
Created on Aug 16, 2019

@author: bkotamahanti
https://leetcode.com/problems/add-to-array-form-of-integer/

For a non-negative integer X, the array-form of X is an array of its digits in left to right order.  For example, if X = 1231, then the array form is [1,2,3,1].

Given the array-form A of a non-negative integer X, return the array-form of the integer X+K.

Example 1:

Input: A = [1,2,0,0], K = 34
Output: [1,2,3,4]
Explanation: 1200 + 34 = 1234
Example 2:

Input: A = [2,7,4], K = 181
Output: [4,5,5]
Explanation: 274 + 181 = 455
Example 3:

Input: A = [2,1,5], K = 806
Output: [1,0,2,1]
Explanation: 215 + 806 = 1021
Example 4:

Input: A = [9,9,9,9,9,9,9,9,9,9], K = 1
Output: [1,0,0,0,0,0,0,0,0,0,0]
Explanation: 9999999999 + 1 = 10000000000

'''
from typing import List

class Solution:
    '''Time: O(max(N,K))
       Space:O(1)'''
    def addToArrayForm1(self, A: List[int], K: int) -> List[int]:
        A.reverse()
        carry=0
        for i in range(0,len(A)):
            K, mod=divmod(K,10)
            carry, A[i]=divmod(A[i]+mod+carry,10)
            K=K+carry
            carry=0
        
        while(K):
            K, mod=divmod(K,10)
            A.append(mod)
        
        
        A.reverse()
        return A
    '''Time:O(max(N,K) )
        Space:O(max(N,K))
    
    '''
    def addToArrayForm(self, A: List[int], K: int) -> List[int]:
        carry = K
        res = []

        for digit in A[::-1]:
            temp = digit + carry
            res.append(temp % 10)
            carry = temp // 10
        while carry:
            res.append(carry % 10)
            carry //= 10

        return res[::-1]
            
        