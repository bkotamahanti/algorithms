'''
Created on Oct 31, 2019

@author: bkotamahanti


Time:O(N^2) 
Space: O(1)
'''

def reverse_words(s):
    l=list(s)
    
    lt=0
    rt=len(l)-1
    
    reverse(l, lt, rt)
    lt=0
    rt=0
    while(rt<len(l)):
        if l[rt]!=' ':
            rt += 1
            continue
        tmp=rt
        rt=rt-1
#         while(lt<rt):
#             l[lt],l[rt] = l[rt],l[lt]
#             lt += 1
#             rt -= 1
        reverse(l,lt,rt)
        lt=tmp+1
        rt=tmp+1    
    rt=rt-1
#     while(lt<rt):
#         l[lt],l[rt] = l[rt],l[lt]
#         lt += 1
#         rt -= 1
    reverse(l, lt, rt)
        
    return "".join(l)

def reverse(l, lt, rt):
    while(lt<rt):
        l[lt],l[rt] = l[rt],l[lt]
        lt += 1
        rt -= 1

print(reverse_words("cake pound steal"))