'''
Created on Nov 21, 2019

@author: bkotamahanti
So I grabbed Apple's stock prices from yesterday and put them in a list called stock_prices, where:

The indices are the time (in minutes) past trade opening time, which was 9:30am local time.
The values are the price (in US dollars) of one share of Apple stock at that time.
Write an efficient function that takes stock_prices and returns the best profit I could have made from one purchase and one sale of one share of Apple stock yesterday.

'''
''' 
Brute_force
Time: O(n^2)
Space: O(1)
'''
def get_max_profit_brute_force(stock_prices):
    if len(stock_prices) < 2:
        raise ValueError("Getting a profit requires at least 2 prices")
    
    max_profit=stock_prices[1]-stock_prices[0]
    for current_time in range(len(stock_prices)):
        for following_time in range(current_time+1, len(stock_prices)):
            if max_profit < stock_prices[following_time]- stock_prices[current_time]:
                max_profit = stock_prices[following_time]- stock_prices[current_time]
    
    return max_profit
'''
OnePass
Time:O(n)
Space:O(1)
he asked max profit by buying and selling one stock.
'''
def get_max_profit(stock_prices):
    if len(stock_prices) < 2:
        raise ValueError('Getting a profit requires at least 2 prices')
    min_price_so_far=float('inf')
    max_profit=stock_prices[1]-stock_prices[0]
    
    for i in range(len(stock_prices)):
        if stock_prices[i] < min_price_so_far:
            min_price_so_far=stock_prices[i]
        else:
            if max_profit < stock_prices[i]-min_price_so_far :
                max_profit=stock_prices[i]- min_price_so_far
    return max_profit
'''
if he askes to find max_profit by doing multiple transactions from yesterday's stocks

'''
def get_max_profit_by_following_peaks_valleys(stock_prices):
    low=0
    high=0
    max_profit=0
    i=0
    n=len(stock_prices)
    # 
    if len(stock_prices) < 2:
        raise ValueError('Getting a profit requires at least 2 prices')

    while(i<n-1):
        while(i<n-1 and stock_prices[i]>stock_prices[i+1]):
            i += 1
        low=stock_prices[i]
        while(i<n-1 and stock_prices[i]<=stock_prices[i+1]):
            i+=1
        high=stock_prices[i]
        if max_profit < (high-low):
            max_profit=high-low
            # print(max_profit)
        
        if i==n-1 and max_profit==0:
            max_profit=stock_prices[1]-stock_prices[0]
        
        
        
    return max_profit

import unittest

class Test(unittest.TestCase):

    def test_price_goes_up_then_down(self):
        actual = get_max_profit([1, 5, 3, 2])
        expected = 4
        self.assertEqual(actual, expected)

    def test_price_goes_down_then_up(self):
        actual = get_max_profit([7, 2, 8, 9])
        expected = 7
        self.assertEqual(actual, expected)
    
    def test_price_goes_down_then_up2(self):
        actual = get_max_profit([7, 1, 5, 3, 6, 4])
        expected = 5
        self.assertEqual(actual, expected)
    
    def test_price_goes_up_all_day(self):
        actual = get_max_profit([1, 6, 7, 9])
        expected = 8
        self.assertEqual(actual, expected)

    def test_price_goes_down_all_day(self):
        actual = get_max_profit([9, 7, 4, 1])
        expected = -2
        self.assertEqual(actual, expected)

    def test_price_stays_the_same_all_day(self):
        actual = get_max_profit([1, 1, 1, 1])
        expected = 0
        self.assertEqual(actual, expected)

    def test_error_with_empty_prices(self):
        with self.assertRaises(Exception):
            get_max_profit([])

    def test_error_with_one_price(self):
        with self.assertRaises(Exception):
            get_max_profit([1])


unittest.main(verbosity=2)
