'''
Created on Oct 14, 2019

@author: bkotamahanti
'''

'''
h=O(n)
Time:O(n!) leaf nodes O(n) to append to output
Space:O(n) stack space
'''
from typing import List
class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        output=[]
        self.permuteRec(nums, 0, output)
        return output
    
    def permuteRec(self, nums, i, output):
        n=len(nums)
        if i==n-1:
            output.append(nums[:])
            return 
        for j in range(i,n):
            nums[i],nums[j]=nums[j],nums[i]
            self.permuteRec(nums, i+1, output)
            nums[i],nums[j]=nums[j],nums[i]
        
        return
        

sol=Solution()
print(sol.permute([1,2,3]))