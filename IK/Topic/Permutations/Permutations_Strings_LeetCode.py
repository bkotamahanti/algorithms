'''
Created on Oct 14, 2019

@author: bkotamahanti
'''

class Solution():
    def permute(self, chars):
        return self.permuteRec(chars, 0)
    
    def permuteRec(self, chars, i):
        n=len(chars)
        if i==n-1:
            print(chars)
            return
        for j in range(0,n):
            chars[i], chars[j] = chars[j], chars[i]
            self.permuteRec(chars, i+1)
            chars[i], chars[j] = chars[j], chars[i]
        return

sol=Solution()
sol.permute(['b','c','e','f'])
