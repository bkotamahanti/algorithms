'''
Created on Oct 21, 2019

@author: bkotamahanti
'''

from typing import List
class Solution:
    def rob(self, nums: List[int]) -> int:
        '''bottom up where i give my cash to parent'''
        return self.recursion1(nums, 0)
        '''top down where i get cash from parent'''
#         return self.recursion(nums, 0, 0)
    
    '''adding from bottom up to find final amount '''
    def recursion1(self, nums, i):
        n=len(nums)
        if i>=n:
            return 0
        temp_amt=0
        max_amt=0
        for j in range(i, n):
            temp_amt=nums[j]+self.recursion1(nums, j+2)
            max_amt=max(max_amt, temp_amt)

        print("max_amt so far:{} for i {} ".format(max_amt,i))
        return max_amt
    '''adding top down DFS ''' 
    def recursion(self, nums, i, amt):
        n=len(nums)
        if i>=n:
            return amt
        temp_amt=0
        max_amt=0
        for j in range(i, n):
            temp_amt=self.recursion(nums, j+2, amt+nums[j])
            max_amt=max(max_amt, temp_amt)

        print("max_amt so far:{} for i {} ".format(max_amt,i))
        return max_amt
    

sol=Solution()
# print(sol.rob([1,2,3]))
print(sol.rob([1,2,3,4]))
print(sol.rob([1,12,3,4,5,6]))
# class Solution:
#     def __init__(self):
#         self.size=0
#         self.max_tot=0
#         self.num_arr=[]
#     def rec_rob(self,ind):
#         if ind<self.size:
#             res=0
#             for j in range(ind+2,self.size):
#                 res=self.num_arr[ind] + self.rec_rob(j)
#             self.max_tot= max(self.max_tot,res)
#             print(str(ind)+"  "+str(self.max_tot))
#             return res
#         else:
#             return 0
#     def rob(self, nums: List[int]) -> int:
#         self.num_arr=nums
#         self.size=len(nums)
#         for m in range(self.size):
#             self.rec_rob(m)
#         return self.max_tot

           
#     #     #      0 1 2 3 4 5 6 7 8
#     # 0 2 4 6 8
#     # 0 2 5 7
#     # 0 2 6 8
#     # 0 2 7 
#     # 0 2 8
#     # 0 3 5 7
#     # 0 3 6 8
#     # 0 3 7 
#     # 0 3 8
#     # 0 4 6 8
#     # 0 4 7
#     # 0 4 8