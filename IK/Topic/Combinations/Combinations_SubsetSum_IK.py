'''
Created on Oct 23, 2019

@author: bkotamahanti
'''
def mainSubsetSum(nums, total):
    s=[0 for _ in range(len(nums))]
    recSubsetSum(nums, 0, s, 0, total)

def recSubsetSum(nums, i, s, j, total):
    n=len(nums)
    if i==n: 
        if sum(s[0:j])==total:
            print(s[:j])
        return
    recSubsetSum(nums, i+1, s, j, total)
    s[j]=nums[i]
    recSubsetSum(nums, i+1, s, j, total)


mainSubsetSum([1,5,6,5,7], 13)
print("=================")
mainSubsetSum([1,5,6,4,2], 6)
    

