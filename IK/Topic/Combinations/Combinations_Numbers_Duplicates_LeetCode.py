'''
Created on Oct 16, 2019

@author: bkotamahanti
'''
from typing import List
class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        '''Time: O(nlogn + 2^n)==> O(2^5)
           Space: O(n + n + height of tree stack space  which is n) ===>O(n)
        '''
        def enumerateSubsets1(nums, i, s, j, output):
            n=len(nums)
            if i==n:
                if tuple(s[:j]) not in _set:
                    _set.add(tuple(s[:j]))
                    output.append(s[0:j])
                return
                
            enumerateSubsets(nums, i+1, s, j, output)
            s[j]=nums[i]
            enumerateSubsets(nums, i+1, s, j+1, output)
        def enumerateSubsets(nums, i, s, j, output):
            n=len(nums)
            if i==n:
                if s[:j]==[] or len(s[:j])==1:
                    t=tuple(s[:j])
                else:
                    s[:j].sort()
                    t=tuple(s[:j])
                    
                if t not in _set:
                    _set.add(t)
                    output.append(s[0:j])
                return
                 
            enumerateSubsets(nums, i+1, s, j, output)
            s[j]=nums[i]
            enumerateSubsets(nums, i+1, s, j+1, output)
#         
#         _set=set()
#         output=[]
#         s=[0 for _ in range(0,len(nums))]
#         enumerateSubsets(nums, 0, s, 0, output)
#         return output
  
        _set=set()
        output=[]
        s=[0 for _ in range(0,len(nums))]
        '''Sort given numbers before enumarating subsets-O(nlogn) in-place'''
        nums.sort()
        enumerateSubsets(nums, 0, s, 0, output)
        return output
    
        
        
    
sol=Solution()
print(sol.subsetsWithDup([4,4,4,1,4]))  
print(sol.subsetsWithDup([1,2,3]))


  