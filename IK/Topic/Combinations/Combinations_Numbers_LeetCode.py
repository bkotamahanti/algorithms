'''
Created on Oct 15, 2019

@author: bkotamahanti

'''
from typing import List
class Solution:
    '''Using class Variable'''
    '''Time: O(2**N.N)
       Space: O(N) call stack will have N stack frames for any given point of time
    '''
    output=[]
    def subsets1(self, nums: List[int]) -> List[List[int]]:
        Solution.output=[]
        s=[0 for _ in range(len(nums))]
        self.recSubsets(nums, 0, s, 0)
        return Solution.output
    
    def recSubsets(self,nums, i , s, j):
        n=len(nums)
        if i==n:
            Solution.output.append(s[0:j]) 
            return 
        self.recSubsets(nums, i+1, s, j)
        s[j]=nums[i]
        self.recSubsets(nums, i+1, s, j+1)
    
    '''Using local variable'''
    def subsets(self, nums: List[int]) -> List[List[int]]:
        output=[]
        def recSubsets(nums, i , s, j):
            n=len(nums)
            if i==n:
                output.append(s[0:j]) 
                return 
            recSubsets(nums, i+1, s, j)
            s[j]=nums[i]
            recSubsets(nums, i+1, s, j+1)
        s=[0 for _ in range(len(nums))]
        recSubsets(nums, 0, s, 0)
        return output
    
    
sol=Solution()
print(sol.subsets([1,2,3]))

        
        
        