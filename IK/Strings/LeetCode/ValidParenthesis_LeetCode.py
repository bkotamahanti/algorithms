'''
Created on Aug 17, 2019

@author: bkotamahanti
'''
class Solution:
    '''Time:O(n)
       Space:O(n)'''
#     def isValid(self, s: str) -> bool:
#         l=[]
#         if len(s)==0:
#             return True
#         n=len(s)
#         for i in range(0,n):
#             if s[i] in ['{','[','(']:
#                 l.append(s[i])
#                 continue
#             if len(l)==0:
#                 return False
#             if s[i]==')':
#                 if l.pop()!='(':
#                     return False
#             if s[i]=='}':
#                 if l.pop()!='{':
#                     return False
#             if s[i]==']':
#                 if l.pop()!='[':
#                     return False
            
#         if not l :
#             return True
#         else:
#             return False
            
    def isValid(self, s: str) -> bool:
        l=[]
        n=len(s)
        if n==0:
            return True
        top=0
        for i in range(n):
            if s[i] in ['{','[','(']:
                l.append(s[i])
                top +=1
                continue
            if len(l)==0:
                return False
            if s[i]==')' and l[top-1]=='(':
                l.pop()
                top -=1
                continue
            elif s[i]=='}' and l[top-1]=='{':
                l.pop()
                top -=1
                continue
            elif s[i]==']' and l[top-1]=='[':
                l.pop()
                top -=1
                continue
            else:
                return False
#         if not l :
#             return True
#         else:
#             return False
        return len(l)==0