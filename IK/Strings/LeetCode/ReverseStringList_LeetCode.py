'''
Created on Sep 2, 2019

@author: bkotamahanti

https://leetcode.com/problems/reverse-string/

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

You may assume all the characters consist of printable ascii characters.
'''
from typing import List

class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        Time:O(n)
        Space:O(1)
        
        l=0
        r=len(s)-1
        while(l<r):
            s[l], s[r] = s[r], s[l]
            l += 1
            r -= 1
        """
        """ 
        Time:O(n)
        Space:O(n)
        
        s2=[]
        for i in range(len(s)-1, -1, -1):
            s2.append(s[i])
        
        return s2
        """
        
        """
        
        """
        n=len(s)
        for i in range(0,n//2):
            temp=s[i]
            s[i]= s[n-1-i]
            s[n-1-i]=temp
        