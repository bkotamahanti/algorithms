'''
Created on Aug 17, 2019

@author: bkotamahanti
Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

Note:

The length of both num1 and num2 is < 5100.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.

"563"
"123"
-----
+686

"-563"
"-123"
------
-686

-563
+123
-----
-440

'''

class Solution:
    '''Time: O(max(n,m))
        Space:O(max(n,m))
    '''
    def addStrings(self, num1: str, num2: str) -> str:
        l=max(len(num1), len(num2))
        k=0
        i=len(num1)-1
        j=len(num2)-1
        carry=0
        result=""
        while(k<l):
            a=0
            b=0
            if(i>=0):
                a=int(num1[i])
                i=i-1
            if(j>=0):
                b=int(num2[j])
                j=j-1
            x=a+b+carry
            carry,x=divmod(x,10)
            result += str(x)
            k +=1
        
        if carry:
            result += str(carry)
        '''O(max(n,m)/2)'''
        return result[::-1]
            
