'''
Created on Sep 6, 2019

@author: bkotamahanti

https://leetcode.com/problems/repeated-string-match/

Given two strings A and B, find the minimum number of times A has to be repeated such that B is a substring of it. If no such solution, return -1.

For example, with A = "abcd" and B = "cdabcdab".

Return 3, because by repeating A three times (“abcdabcdabcd”), B is a substring of it; and B is not a substring of A repeated two times ("abcdabcd").

Note:
The length of A and B will be between 1 and 10000.

'''

class Solution:
    def repeatedStringMatch(self, A: str, B: str) -> int:
        ''' 
        if you want to compare with char to char instead of compromising on space then time will shoot up but still possible. However you need to assume the
        pattern string will be long string then in that case it is better to compromise with space and time will come down.
        
        the below solution is using more Space. 
        
        abcd    abc count=1
        abcd    ab  count=1
        abcd    ""  count=1
        abcd  abcdabcdab   abcd|abcd|ab count=3
        abcd  cdabcdab   abcd|abcd|abcd count=3
        abc|abc|abc|abc" "abc" count=1 
        cbc|abacaba|      abac count=1
        cbc|abcaba|      abac count=1 abcabcabacbcabcaba
        
        aabaaabaaac aabaaac
        abc cabcabca
        '''
        count=1
        inpu=A
        while len(A)>len(B) and self.subString(A,B)==False:
            if count==1:
                A=A+inpu
                count+=1
                continue
                
            return -1
        
        
        while(count<=len(B)):
            if self.subString(A,B):
                return count
            A+=inpu
            count+=1
            print(count)
            print(A)
        if count == len(B)+1:
            return -1
        return count
    
    def subString(self, A, B):
        return B in A
        
    '''In this we are not compromising with Space. hence time shoots up and Leetcode has time limit.'''      
    def repeatedStringMatch1(self, A: str, B: str) -> int:   
        if B=="":
            return 1
        # if A==B:
        #     return 1
        # if A[:]==B[0:len(B)/2] and A[:]==B[len(B)/2:]:
        #     return 2
        i=0
        j=0
        n=len(A)
        count=0
        start=-1
        while(j<len(B)):
            i=0
            while(i<n and j<len(B)):
                if A[i]==B[j]:
                    if start == -1:
                        start=i
                    i+=1
                    j+=1
                    continue

                if j==0:
                    i+=1
                    start=i
                    continue
                if j!=0:
                    j=0
                    i=(start+1)%len(A)
                    start=-1
                
            count +=1
            if count > len(B):
                return -1
        
        result= -1 if count==0 else count
        return result
