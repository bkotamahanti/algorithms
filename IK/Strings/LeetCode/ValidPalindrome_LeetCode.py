'''
Created on Aug 19, 2019

@author: bkotamahanti

https://leetcode.com/problems/valid-palindrome/

Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

Note: For the purpose of this problem, we define empty string as valid palindrome.

Example 1:

Input: "A man, a plan, a canal: Panama"
Output: true
Example 2:

Input: "race a car"
Output: false
'''
class Solution:
    def isPalindrome(self, s: str) -> bool:
        if s=="":
            return True
        import re
        # l=re.findall(r"[^\w]",s)
        # l=re.findall(r"[^A-Za-z0-9]",s)
        # print(l)
        s=re.sub("[^a-zA-z0-9]","",s)
        i=0;
        j=len(s)-1
        while(i<j):
            # if s[i] in l:
            #     i+=1
            #     continue
            # if s[j] in l:
            #     j-=1
            #     continue
            if s[i].lower()!=s[j].lower():
                return False
            i +=1
            j -=1
        
        return True
    '''Time limit exceeded since re.sub goes through O(n) chars if the string length is more 
    we get time limit exceeding error. To avoid that use the string as it is with 2 pointer approach
     '''
    def isPalindrome1(self, s: str) -> bool:
        if s=="":
            return True
        
        import string
        l=set(string.punctuation)
        l.add(" ")
        print(l)
        # import re
        # l=re.findall(r"[^\w]",s)
        # l=re.findall(r"[^A-Za-z0-9]",s)
        # print(l)
        # s=re.sub("[^a-zA-z0-9]","",s)
        i=0;
        j=len(s)-1
        while(i<j):
            if(s[i] in l):
                i+=1
                continue
            if(s[j] in l):
                j-=1
                continue
            if s[i].lower()!=s[j].lower():
                return False
            i +=1
            j -=1
        
        return True