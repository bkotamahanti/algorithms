import itertools
class Solution:
    ''' Using stack approach
        Time: O(m+n)
       Space: O(m+n)'''
    def backspaceCompare(self, S: str, T: str) -> bool:
        def buildString(s):
            ans = []
            for c in s:
                if c != '#':
                    ans.append(c)
                elif ans:
                    ans.pop()
            return ans
        
        return  buildString(S)==buildString(T)
    
    '''Time: O(m+n)'''
    def backspaceCompare1(self, S, T):
        def F(S):
            skip = 0
            for x in reversed(S):
                if x == '#':
                    skip += 1
                elif skip:
                    skip -= 1
                else:
                    yield x

        return all(x == y for x, y in itertools.zip_longest(F(S), F(T)))
    
    def backspaceCompare2(self, S, T): 
        i=len(S)-1
        j=len(T)-1
        skip_i=0
        skip_j=0
        while(i>=0 or j>=0):
            while(i>=0):
                if S[i]=='#':
                    skip_i += 1
                    i -= 1
                elif skip_i:
                    skip_i -= 1
                    i -= 1
                else:
                    break
            
            while(j>=0):
                if T[j]=='#':
                    skip_j += 1
                    j -= 1
                elif skip_j:
                    skip_j -= 1
                    j -= 1
                    
                else:
                    break
            
            if (i>=0 and j>=0 and S[i]!=T[j]):
                return False
            
            if (i>=0) != (j>=0):
                return False
            i -=1
            j -=1
        
        return True
                
                
                    
            
    
sol=Solution()
print(sol.backspaceCompare2("ab#c", "agg##c"))
print(sol.backspaceCompare2("ab##", "cc###"))
        
    