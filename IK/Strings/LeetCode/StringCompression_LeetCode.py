'''
Created on Nov 8, 2019

@author: bkotamahanti
'''
from typing import List
class Solution:
    ''' 
    input:list of chars 
    output: int (len of new chars)
    the new len should be < actual len
    
    '''
    def compress(self, chars: List[str]) -> int:
        def checkCount():
            nonlocal k
            nonlocal count
            nonlocal chars
            if count>1:
                k+=1
                s=str(count)
                if len(s)==1:
                    chars[k]=s
                else:
                    for c in str(count):
                        chars[k]=c
                        k+=1
                    k=k-1
                count=1
                
                
        i=0
        j=i+1
        k=0
        count=1
        while(j<len(chars)):
            if chars[i]==chars[j]:
                j += 1
                count+=1
                continue
            
            chars[k]=chars[i]
            checkCount()
            k+=1
            i=j
            j=i+1
        
        chars[k]=chars[i]
        checkCount()        
        return k+1
    
    
    def compress1(self, chars: List[str]) -> int:
        def checkCount():
            nonlocal k
            nonlocal count
#             nonlocal chars
            if count>1:
                k+=1
                while(count>=0):
                    div=count//10
                    if div==0:
                        chars[k]=str(count%10)
                        break
    
                    chars[k]=str(div)
                    k+=1
                    count=count%10
                count=1
        
        
        i=0
        j=i+1
        k=0
        count=1
        while(j<len(chars)):
            if chars[i]==chars[j]:
                j += 1
                count+=1
                continue
            
            chars[k]=chars[i]
            checkCount()
            k+=1
            i=j
            j=i+1
        
        chars[k]=chars[i]
        checkCount()        
        return k+1
    
sol=Solution()
# chars=["a","b","b","b","b","b","b","b","b","b","b","b","b"]
chars=["a","a","b","b","c","c","c"]
# print(sol.compress(chars))
for i in range(sol.compress(chars)):
    print(chars[i])
