'''
Created on Aug 17, 2019

@author: bkotamahanti
Given two integers num1 and num2 of same length or len(num2)=len(num1)-1 or vice versa represented as string, return the sum of num1 and num2.

Note:

The length of both num1 and num2 is < 5100.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.

"563"
"123"
-----
+686

"-563"
"-123"
------
-686

-563
+123
-----
-440

-563
623
------

623
-563

'''

class Solution:
    '''Time: O(max(n,m))
        Space:O(max(n,m))
    '''
    def addStrings(self, num1: str, num2: str) -> str:
        num1_pos=False
        num2_pos=False
        sign=""
        l=max(len(num1), len(num2))
        k=0
        i=len(num1)-1
        j=len(num2)-1
        carry=0
        result=""
        if (num1[k] != "-"):
            num1_pos=True
        if (num2[k] != "-"):
            num2_pos=True
        if num1_pos and num2_pos:
            sign=""
        if not num1_pos and not num2_pos:
            sign="-"
        if (not num1_pos and num2_pos):
            if isgreater(num1,num2):
                a=num1
                b=num2
                sign="-"
            else:
                a=num2
                b=num1
                sign=""
        if (num1_pos and not num2_pos):
            if isgreater(num1,num2):
                a=num1
                b=num2
                sign=""
            else:
                a=num2
                b=num1
                sign="-"
        while(k<l):
            a=0
            b=0
            if(i>=0):
                a=int(num1[i])
                i=i-1
            if(j>=0):
                b=int(num2[j])
                j=j-1
            x=a+b+carry
            carry,x=divmod(x,10)
            result += str(x)
            k +=1
        
        if carry:
            result += str(carry)
        '''O(max(n,m)/2)'''
        return result[::-1]
            

