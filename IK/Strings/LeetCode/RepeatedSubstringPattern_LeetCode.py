'''
Created on Sep 5, 2019

@author: bkotamahanti
https://leetcode.com/problems/repeated-substring-pattern/

Given a non-empty string check if it can be constructed by taking a substring of it and appending multiple copies of the substring together. 
You may assume the given string consists of lowercase English letters only and its length will not exceed 10000.

'''

class Solution:
    '''this solution is based on dictionary and works for test cases
    abcabcabc True
    abcabc  True
    abc False
    "" True
    abcab False
    but fails for
    abba false
    Time:O(n)
    Space:O(n) considering worst case 
    
    '''
    def repeatedSubstringPattern1(self, s: str) -> bool:
        import collections
        letters=collections.Counter(s)
        s=set(letters.values())
        if len(s)> 1:
            return False
        else:
            element=s.pop()
            return False if element==1 else True
    
    def repeatedSubstringPattern(self, s: str) -> bool:
        ''' 
        Time:O(2n)
        Space:O(2n)
        '''
        ss=s+s
        return True if  ss[1:len(ss)-1].find(s)!=-1 else False
    
        
        
