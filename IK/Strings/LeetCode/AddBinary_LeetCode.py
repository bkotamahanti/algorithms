'''
Created on Aug 16, 2019

@author: bkotamahanti

https://leetcode.com/problems/add-binary/

Given two binary strings, return their sum (also a binary string).

The input strings are both non-empty and contains only characters 1 or 0.

Example 1:

Input: a = "11", b = "1"
Output: "100"
'''

from itertools import zip_longest  
class Solution:
#     def addBinary(self, a: str, b: str) -> str:
#         i=len(a)-1
#         j=len(b)-1
#         res=""
#         carry=0
#         while(i>=0 or j>=0):
#             a1=0
#             b1=0
#             if i>=0:
#                 a1=a[i]
#                 i=i-1
#             if j>=0:
#                 b1=b[j]
#                 j=j-1
                
#             x=int(a1)+int(b1)+carry
#             res= res+str(x%2)
#             carry=x//2
        
#         if carry!=0:
#             res=res+str(carry)
        
#         return res[::-1]

    def addBinary(self, a: str, b: str) -> str:
        result = ""
        carry = 0
        for da, db in zip_longest(reversed(a), reversed(b), fillvalue=0):
            do = int(da) + int(db) + carry
            carry, do = divmod(do, 2)
            result += str(do)
            
        if carry:
            result += str(carry)
            
        return result[::-1]            

