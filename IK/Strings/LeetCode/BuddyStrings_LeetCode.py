'''
https://leetcode.com/problems/buddy-strings/
Given two strings A and B of lowercase letters, return true if and only if we can swap two letters in A so that the result equals B.
ab ba
aa aa
ab ab false
aaabc  acaba true
aba aba true
acbaz abcad
"" ca
'''
from itertools import zip_longest
class Solution:
    def buddyStrings(self, A: str, B: str) -> bool:
        if len(A)!=len(B): return False
        flag=False
        l=[]
        for a,b in zip_longest(A,B):
            if a!=b:
                flag=True
                l.append((a,b))
        
        if flag:
            if len(l)>2:
                return False
            else:
                return len(l)==2 and l[0]==l[1][::-1]
        else:
            seen=set()
            for a in A:
                if a in seen:
                    return True
                seen.add(a)
            return False
sol=Solution()
print(sol.buddyStrings("ab", "ba"))
print(sol.buddyStrings("ab", "ab"))
print(sol.buddyStrings("abca", "abca"))
print(sol.buddyStrings("", "ba"))
print(sol.buddyStrings("aba", "aba"))
print(sol.buddyStrings("abz", "bat"))        
        
        
class Solution1:
    def buddyStrings(self, A, B):
        if len(A) != len(B):
            return False
        differences = []
        for idx in range(len(A)):
            if A[idx] != B[idx]:
                differences.append(idx)
                if len(differences) > 2:
                    return False

        if len(differences) == 1:
            return False
        elif len(differences) == 0:
            _dict = {}
            for char in A:
                if char not in _dict.keys():
                    _dict[char] = 1
                else:
                    return True
        else:
            return A[differences[0]] == B[differences[1]] and A[differences[1]] == B[differences[0]]      
        
        
        
        
        
        
        
        
        
      
        