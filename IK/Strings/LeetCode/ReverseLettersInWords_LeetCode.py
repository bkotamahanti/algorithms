'''
Created on Sep 1, 2019

@author: bkotamahanti

https://leetcode.com/problems/reverse-words-in-a-string-iii/
Reverse Words in a String III
Easy

680

71

Favorite

Share
Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

Example 1:
Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"
'''
class Solution:
    ''' Time:O(n)
        Space:O(n)'''
    def reverseWords2(self, s: str) -> str:
        words=s.split(" ")
        count=0
        for word in words:
            words[count]=word[::-1]
            count +=1
        return " ".join(words)
    ''' Time: O(n)
        Space: O(n)'''
    def reverseWords(self, s: str) -> str:
        i=j=0
        l=list(s)
        
        for j in range(0, len(l)):
            if l[j] != ' ':
                continue
            l[i:j]=l[i:j][::-1] #here we are using assignment of list rather than below swapping. swapping takes more time than assignment.
            # k=j-1
            # while(i<k):
            #     l[i],l[k]=l[k],l[i]
            #     i+=1
            #     k-=1
            i=j+1
        l[i:j+1]=l[i:j+1][::-1]
        return "".join(l)        
            
        
