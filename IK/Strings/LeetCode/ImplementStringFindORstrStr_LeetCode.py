'''
Created on Sep 5, 2019

@author: bkotamahanti
https://leetcode.com/problems/implement-strstr/
Implement strStr().

Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

Example 1:

Input: haystack = "hello", needle = "ll"
Output: 2
'''

class Solution:
    ''' 
     hello ll 2
     hello o  4
     hello ab -1
     a  a 0
     "" abc -1
     "" ""  0
    Time : O(n)
    Space: O(1)
    '''
    def strStr(self, haystack: str, needle: str) -> int:
        for i in range(len(haystack)):
            if needle==haystack[i:i+len(needle)]:
                return i
        
        return 0 if needle=="" else -1
    
    def strStr1(self, haystack: str, needle: str) -> int:
        for i in range(len(haystack)):
            j=i+len(needle)
            if j > len(haystack):
                j=len(haystack)
                
                
            if needle==haystack[i:j]:
                return i
        
        return 0 if needle=="" else -1
            

sol=Solution()
print(sol.strStr("hello", "ab"))
