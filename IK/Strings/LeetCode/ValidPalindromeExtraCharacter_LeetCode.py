'''
Created on Aug 19, 2019

@author: bkotamahanti
https://leetcode.com/problems/valid-palindrome-ii/

Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.

Example 1:
Input: "aba"
Output: True
Example 2:
Input: "abca"
Output: True
Explanation: You could delete the character 'c'.

'''

class Solution:
    '''Time:O(n)
       Space:O(N/2)'''
    def validPalindrome(self, s: str) -> bool:
        i=0
        j=len(s)-1
        l=[]
        forward=False
        backward=False
        while(i<j):
            if s[i]==s[j]:
                i+=1
                j-=1
                continue
            if s[i]!=s[j]:
                if not forward:
                    forward=True
                    l.append(s[i])
                    i+=1
                    continue
                elif not backward:
                    backward=True
                    l.pop()
                    l.append(s[j])
                    i-=1
                    j-=1
                    continue
                else:
                    l.append(s[i])
                    break
                
        return len(l)<=1
                
        