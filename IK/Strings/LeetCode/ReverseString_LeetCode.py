'''
Created on Aug 30, 2019

@author: bkotamahanti

'''
class Solution:
    '''Time:O(n-n/2) ie., O(n)
       Space:O(n)+O(n) ie O(n)
    '''
    def reverseStr1(self, s: str, k: int) -> str:
        l=list(s)
        n=len(l)
        for start in range(0, n, 2*k):
            i=start
            j=min(start+k-1, n-1)
            
            while(i<j):
                l[i], l[j] = l[j], l[i]
                i+=1
                j-=1
        return "".join(l)
    
    def reverseStr(self, s: str, k: int )->str:
        l=list(s)
        for i in range(0, len(l), 2*k):
            l[i:i+k]=l[i:i+k][::-1]
            # l[i:i+k]=reversed(l[i:i+k])
            
        return "".join(l)   
        
        