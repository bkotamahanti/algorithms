'''
Created on Dec 9, 2019

@author: bkotamahanti
'''
from IK.Sorting.Heaps.HeapConcept import Heap
def heapSort(arr, h):
    h.buildMaxHeap(arr)
#     h.printHeap(arr)
    n=len(arr)
    
    for idx in range(n-1, -1, -1):
        temp=arr[0]
        arr[0]=arr[idx]
        arr[idx]=temp
        h.heapifyMax(arr, idx, 0)
    
    return arr

arr=[1, 3, 5, 4, 6, 13, 10, 9, 8, 15, 17]
h=Heap()
print(heapSort(arr, h))
# print(arr)