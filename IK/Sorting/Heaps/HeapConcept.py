class Heap():
    def heapifyMax(self, arr, n, idx):
        max_idx_so_far=idx
        l=2*idx + 1
        r=2*idx + 2
        
        # If left child is larger than root 
        if (l < n and arr[l] > arr[max_idx_so_far] ):
            max_idx_so_far = l; 
      
        # If right child is larger than largest so far 
        if (r < n and arr[r] > arr[max_idx_so_far]): 
            max_idx_so_far = r; 
            
        if (idx!=max_idx_so_far):
            arr[idx], arr[max_idx_so_far]=arr[max_idx_so_far], arr[idx]
            self.heapifyMax(arr, n, max_idx_so_far)
    
    
    def heapifyMin(self, arr, n, idx):
        min_idx_so_far=idx
        l=2*idx + 1
        r=2*idx + 2
        
        # If left child is larger than root 
        if (l < n and arr[l] < arr[min_idx_so_far] ):
            min_idx_so_far = l; 
      
        # If right child is larger than largest so far 
        if (r < n and arr[r] < arr[min_idx_so_far]): 
            min_idx_so_far = r; 
            
        if (idx!=min_idx_so_far):
            arr[idx], arr[min_idx_so_far]=arr[min_idx_so_far], arr[idx]
            self.heapifyMin(arr, n, min_idx_so_far)
        
    def buildMaxHeap(self, arr):
        n=len(arr)
        # Index of last non-leaf node as the leaf nodes already satisfy heap-property
        start_idx=n//2 -1
        
        # Perform reverse level order traversal 
        # from last non-leaf node and heapifyMax 
        # each node 
        for i in range(start_idx, -1, -1): 
            self.heapifyMax(arr, n, i); 
    
    def buildMinHeap(self, arr):
        n=len(arr)
        # Index of last non-leaf node as the leaf nodes already satisfy heap-property
        start_idx=n//2 -1
        
        # Perform reverse level order traversal 
        # from last non-leaf node and heapifyMax 
        # each node 
        for i in range(start_idx, -1, -1): 
            self.heapifyMin(arr, n, i); 
        
    
    def printHeap(self, arr):
        print(arr)


arr=[1, 3, 5, 4, 6, 13, 10, 9, 8, 15, 17]
h=Heap()
h.buildMaxHeap(arr)
# h.printHeap(arr)

h.buildMinHeap(arr)
# h.printHeap(arr)